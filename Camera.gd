extends Camera

const SPEED = 1

var mouse_sensitivity = 0.08
var movement = [false, false, false, false]
var active = true


func _process(delta: float) -> void:
	if not active:
		return
	if movement[0]:
		transform.origin -= transform.basis.z * SPEED * delta
	elif movement[1]:
		transform.origin += transform.basis.z * SPEED * delta
	if movement[2]:
		transform.origin += transform.basis.x * SPEED * delta
	elif movement[3]:
		transform.origin -= transform.basis.x * SPEED * delta


func _input(event) -> void:
	if not active:
		return
	if event is InputEventMouseMotion:
		rotation_degrees = Vector3(
			clamp(rotation_degrees.x - (event.relative.y * mouse_sensitivity),-90, 90),
			rotation_degrees.y - (event.relative.x * mouse_sensitivity),
			0.0)
	elif event is InputEventKey:
		if event.is_echo():
			return
		if event.is_pressed():
			if event.is_action("ui_up"):
				movement[0] = true
			if event.is_action("ui_down"):
				movement[1] = true
			if event.is_action("ui_right"):
				movement[2] = true
			if event.is_action("ui_left"):
				movement[3] = true
		else:
			if event.is_action("ui_up"):
				movement[0] = false
			if event.is_action("ui_down"):
				movement[1] = false
			if event.is_action("ui_right"):
				movement[2] = false
			if event.is_action("ui_left"):
				movement[3] = false
