extends Control


class ComImageData:
	var raw_image: PoolByteArray
	var seg_image: PoolByteArray
	var top_faces: Array
	var corners: Array

	func to_dict() -> Dictionary:
		return {
			'raw': raw_image,
			'seg': seg_image,
			'top_faces': top_faces,
			'corners': corners}


class ComData:
	var top_faces: Array
	var corners: Array

	func to_dict() -> Dictionary:
		return {'top_faces': top_faces, 'corners': corners}


var com: UDPCom
var com_thread: Thread = Thread.new()
var com_initialized: bool = false
enum IMAGE_REQUEST {DONE, RAW, SEGMENTATION}
var image_requested = IMAGE_REQUEST.DONE
var com_data = ComImageData.new()
var com_error
var raw_memory: File = null
var seg_memory: File = null
var camera_image: Image
var with_seg: bool = false


onready var view = $Viewport
onready var table = $Viewport/TableScene


func _ready() -> void:
	com = UDPCom.new()
	com.pool_usec_delay = 10
	if com.connect("com_init", self, "_on_com_init", [], CONNECT_ONESHOT) != OK:
		print('GODOT : Cannot connect to communication signal')
	if com_thread.start(com, "start", null) != OK:
		print('GODOT : Cannot start communication')

	table.piloted = true


func _process(_delta: float) -> void:
	if com_initialized:
		if image_requested == IMAGE_REQUEST.RAW:
			com_data.top_faces = table.dice_top_faces
			if table.detection_mode:
				table.get_corners()
				com_data.corners = table.corners
			camera_image = view.get_texture().get_data()
			if raw_memory != null:
				raw_memory.seek(0)
				raw_memory.store_buffer(camera_image.get_data())
				raw_memory.flush()
			else:
				com_data.raw_image = camera_image.get_data()
			if with_seg:
				image_requested = IMAGE_REQUEST.SEGMENTATION
				table.toggle_segmentation()
				return
			else:
				image_requested = IMAGE_REQUEST.DONE
				com_error = com.send_message(
					ComMessage.Response.new(ComMessage.OK, com_data.to_dict()))
		elif image_requested == IMAGE_REQUEST.SEGMENTATION:
			image_requested = IMAGE_REQUEST.DONE
			camera_image = view.get_texture().get_data()
			if seg_memory != null:
				seg_memory.seek(0)
				seg_memory.store_buffer(camera_image.get_data())
				seg_memory.flush()
				table.toggle_segmentation()
			else:
				com_data.seg_image = camera_image.get_data()
			com_error = com.send_message(ComMessage.Response.new(
				ComMessage.OK, com_data.to_dict()))

		var request = com.receive(-1)
		if request == null :
			pass
		elif request.action == 'init':
			with_seg = request.data['seg']
			if 'size' in request.data:
				view.size = Vector2(request.data['size'][0], request.data['size'][1])
			if 'camera_distance' in request.data:
				table.min_camera_distance = request.data['camera_distance'][0]
				table.max_camera_distance = request.data['camera_distance'][1]
			if 'detection' in request.data:
				table.detection_mode = bool(request.data['detection'])
				table.dice_top_faces = [0, 0]
			else:
				table.dice_top_faces = [0]
			camera_image = view.get_texture().get_data()
			com_error = com.send_message(
				ComMessage.Response.new(ComMessage.OK, camera_image.get_size()))
		elif request.action == 'shm':
			var raw_memory_name = request.data['raw']
			com_data = ComData.new()
			if raw_memory_name != null:
				raw_memory = File.new()
				raw_memory.open(raw_memory_name, File.READ_WRITE)
				if with_seg:
					seg_memory = File.new()
					seg_memory.open(request.data['seg'], File.READ_WRITE)
			com_error = com.send_message(ComMessage.Response.new(ComMessage.OK, null))
		elif request.action == 'set':
			table.difficulty_ratio = clamp(request.data['difficulty'], 0.0, 1.0)
			com_error = com.send_message(
				ComMessage.Response.new(ComMessage.OK, camera_image.get_size()))
		elif request.action == 'img':
			image_requested = IMAGE_REQUEST.RAW
			table.shuffle_all()
		else:
			if raw_memory != null:
				raw_memory.close()
			if seg_memory != null:
				seg_memory.close()
			com_error = com.send_message(ComMessage.Response.new(ComMessage.OK, null))
			com_initialized = false
			com.stop()
			table.toggle_generator()
			com_thread.wait_to_finish()
			com = UDPCom.new()


func _on_com_init() -> void:
	com_initialized = true
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	table.toggle_generator()
