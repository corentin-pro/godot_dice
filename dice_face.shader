shader_type spatial;
render_mode blend_mix,cull_disabled,diffuse_burley,specular_schlick_ggx,shadows_disabled;
uniform vec4 albedo : hint_color;
uniform vec4 noise_albedo : hint_color;
uniform vec4 circle_albedo : hint_color;
uniform float circle_radius : hint_range(0,1);
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float circle_distance : hint_range(0,1);

const float STRETCH_CONSTANT_2D = -0.211324865405187; // (1 / sqrt(2 + 1) - 1 ) / 2;
const float SQUISH_CONSTANT_2D = 0.366025403784439; // (sqrt(2 + 1) -1) / 2;
const float NORM_CONSTANT_2D = 47.0;

uniform int face;
uniform float period;
uniform int octaves;
uniform float lacunarity;
uniform float persistence;


vec3 permute(vec3 x)
{
	return mod(((x * 34.0) + 1.0) * x, 289.0);
}

vec3 taylorInvSqrt(vec3 r)
{
	return 1.79284291400159 - 0.85373472095314 * r;
}

float snoise(vec2 P)
{
	const vec2 C = vec2(
		0.211324865405187134, // (3.0 - sqrt(3.0)) / 6.0;
		0.366025403784438597); // 0.5*( sqrt(3.0) - 1.0);
	// First  corner
	vec2 i = floor(P + dot(P, C.yy));
	vec2 x0 = P - i + dot(i, C.xx);
	// Other  corners
	vec2 i1;
	i1.x = step(x0.y, x0.x); // 1.0 if x0.x > x0.y, else 0.0
	i1.y = 1.0 - i1.x;
	// x1 = x0 - i1 + 1.0 * C.xx;  x2 = x0 - 1.0 + 2.0 * C.xx;
	vec4 x12 = x0.xyxy + vec4(C.xx, C.xx * 2.0 - 1.0);
	x12.xy -= i1;
	// Permutations
	i= mod(i, 289.0);  // Avoid truncation in polynomial evaluation
	vec3 p = permute(permute(i.y + vec3(0.0, i1.y, 1.0))
		+ i.x + vec3(0.0, i1.x, 1.0));
	// Circularly  symmetric  blending  kernel
	vec3 m = max(
		0.5 - vec3(dot(x0, x0), dot(x12.xy, x12.xy) , dot(x12.zw, x12.zw)),
		0.0);
	m = m * m;
	m = m * m;
	//  Gradients  from 41  points  on a  line ,  mapped onto a diamond
	vec3 x = fract(p * (1.0 / 41.0)) * 2.0 - 1.0;
	vec3 gy = abs(x) - 0.5;
	vec3 ox = round(x);
	vec3 gx = x - ox;
	// Normalise gradients implicitly by scaling m
	m *= taylorInvSqrt(gx * gx + gy * gy);
	// Compute final noise value at P
	vec3 g;
	g.x = gx.x * x0.x + gy.x * x0.y;
	g.yz = gx.yz * x12.xz + gy.yz * x12.yw;
	// Scale output to span range [-1 ,1] (scaling factor determined by experiments)
	return 130.0 * dot(m,g);
}

float simplex_noise_2d(vec2 loc)
{
	loc /= period;

	float amp = 1.0;
	float max = 1.0;
	float sum = snoise(loc);

	int i = 0;
	while (++i < octaves) {
		loc.x *= lacunarity;
		loc.y *= lacunarity;
		amp *= persistence;
		max += amp;
		sum += snoise(loc) * amp;
	}

	return sum / max;
}

bool check_face_1(vec2 loc)
{
	return length(loc - vec2(0.5, 0.5)) < circle_radius;
}

bool check_face_2(vec2 loc)
{
	float diag_distance = (0.33333 * circle_distance);
	return length(loc - vec2(0.5 - diag_distance, 0.5 - diag_distance)) < circle_radius
		|| length(loc - vec2(0.5 + diag_distance, 0.5 + diag_distance)) < circle_radius;
}

bool check_face_3(vec2 loc)
{
	float diag_distance = (0.33333 * circle_distance);
	return length(loc - vec2(0.5 - diag_distance, 0.5 - diag_distance)) < circle_radius
		|| length(loc - vec2(0.5, 0.5)) < circle_radius
		|| length(loc - vec2(0.5 + diag_distance, 0.5 + diag_distance)) < circle_radius;
}

bool check_face_4(vec2 loc)
{
	float diag_distance = (0.33333 * circle_distance);
	return length(loc - vec2(0.5 - diag_distance, 0.5 - diag_distance)) < circle_radius
		|| length(loc - vec2(0.5 - diag_distance, 0.5 + diag_distance)) < circle_radius
		|| length(loc - vec2(0.5 + diag_distance, 0.5 - diag_distance)) < circle_radius
		|| length(loc - vec2(0.5 + diag_distance, 0.5 + diag_distance)) < circle_radius;
}

bool check_face_5(vec2 loc)
{
	float diag_distance = (0.33333 * circle_distance);
	return length(loc - vec2(0.5, 0.5)) < circle_radius
		|| length(loc - vec2(0.5 - diag_distance, 0.5 - diag_distance)) < circle_radius
		|| length(loc - vec2(0.5 - diag_distance, 0.5 + diag_distance)) < circle_radius
		|| length(loc - vec2(0.5 + diag_distance, 0.5 - diag_distance)) < circle_radius
		|| length(loc - vec2(0.5 + diag_distance, 0.5 + diag_distance)) < circle_radius;
}

bool check_face_6(vec2 loc)
{
	float diag_distance = (0.33333 * circle_distance);
	return length(loc - vec2(0.5 - diag_distance, 0.5)) < circle_radius
		|| length(loc - vec2(0.5 + diag_distance, 0.5)) < circle_radius
		|| length(loc - vec2(0.5 - diag_distance, 0.5 - diag_distance)) < circle_radius
		|| length(loc - vec2(0.5 - diag_distance, 0.5 + diag_distance)) < circle_radius
		|| length(loc - vec2(0.5 + diag_distance, 0.5 - diag_distance)) < circle_radius
		|| length(loc - vec2(0.5 + diag_distance, 0.5 + diag_distance)) < circle_radius;
}

void fragment() {
	bool in_circle = false;
	switch(face)
	{
		case 1:
			in_circle = check_face_1(UV.xy);
			break;
		case 2:
			in_circle = check_face_2(UV.xy);
			break;
		case 3:
			in_circle = check_face_3(UV.xy);
			break;
		case 4:
			in_circle = check_face_4(UV.xy);
			break;
		case 5:
			in_circle = check_face_5(UV.xy);
			break;
		case 6:
			in_circle = check_face_6(UV.xy);
			break;
	}

	if(in_circle)
	{
		ALBEDO = circle_albedo.rgb;
		ALPHA = circle_albedo.a;
		METALLIC = 0.0;
		ROUGHNESS = 0.8;
		SPECULAR = 0.2;
	}
	else
	{
		float noise_value = simplex_noise_2d(UV);
		ALBEDO = mix(albedo.rgb, noise_albedo.rgb, noise_value);
		ALPHA = albedo.a + ((1.0 - albedo.a) * noise_value);
		METALLIC = metallic;
		ROUGHNESS = roughness;
		SPECULAR = specular;
	}
}
