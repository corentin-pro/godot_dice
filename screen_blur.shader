shader_type canvas_item;

uniform float blur_amount : hint_range(0, 5);
uniform float noise : hint_range(0, 1);

vec3 white_noise(vec2 pos)
{
	return vec3(
		fract(abs(sin(dot(pos, vec2(12.989, 78.233))) * 143758.5453)),
		fract(abs(sin(dot(pos, vec2(39.346, 11.135))) * 143758.5453)),
		fract(abs(sin(dot(pos, vec2(08.156, 52.235))) * 143758.5453)));
}

void fragment() {
	COLOR = vec4(mix(
		clamp(textureLod(SCREEN_TEXTURE, SCREEN_UV, blur_amount).rgb, vec3(0.0), vec3(1.0)),
		white_noise(SCREEN_UV), noise),
		1.0);
}