shader_type spatial;
render_mode unshaded,cull_back,shadows_disabled;

uniform float circle_radius;
uniform int face;

varying vec3 global_normal;


bool check_face_1(vec2 loc)
{
	return length(loc - vec2(0.5, 0.5)) < circle_radius;
}

bool check_face_2(vec2 loc)
{
	return length(loc - vec2(0.166667, 0.166667)) < circle_radius
		|| length(loc - vec2(0.833333, 0.833333)) < circle_radius;
}

bool check_face_3(vec2 loc)
{
	return length(loc - vec2(0.166667, 0.166667)) < circle_radius
		|| length(loc - vec2(0.5, 0.5)) < circle_radius
		|| length(loc - vec2(0.833333, 0.833333)) < circle_radius;
}

bool check_face_4(vec2 loc)
{
	return length(loc - vec2(0.166667, 0.166667)) < circle_radius
		|| length(loc - vec2(0.166667, 0.833333)) < circle_radius
		|| length(loc - vec2(0.833333, 0.166667)) < circle_radius
		|| length(loc - vec2(0.833333, 0.833333)) < circle_radius;
}

bool check_face_5(vec2 loc)
{
	return length(loc - vec2(0.5, 0.5)) < circle_radius
		|| length(loc - vec2(0.166667, 0.166667)) < circle_radius
		|| length(loc - vec2(0.166667, 0.833333)) < circle_radius
		|| length(loc - vec2(0.833333, 0.166667)) < circle_radius
		|| length(loc - vec2(0.833333, 0.833333)) < circle_radius;
}

bool check_face_6(vec2 loc)
{
	return length(loc - vec2(0.166667, 0.5)) < circle_radius
		|| length(loc - vec2(0.833333, 0.5)) < circle_radius
		|| length(loc - vec2(0.166667, 0.166667)) < circle_radius
		|| length(loc - vec2(0.166667, 0.833333)) < circle_radius
		|| length(loc - vec2(0.833333, 0.166667)) < circle_radius
		|| length(loc - vec2(0.833333, 0.833333)) < circle_radius;
}


void vertex() {
	global_normal = (WORLD_MATRIX * vec4(NORMAL, 0.0)).xyz;
}


void fragment() {
	bool in_circle = false;
	switch(face)
	{
		case 1:
			in_circle = check_face_1(UV.xy);
			break;
		case 2:
			in_circle = check_face_2(UV.xy);
			break;
		case 3:
			in_circle = check_face_3(UV.xy);
			break;
		case 4:
			in_circle = check_face_4(UV.xy);
			break;
		case 5:
			in_circle = check_face_5(UV.xy);
			break;
		case 6:
			in_circle = check_face_6(UV.xy);
			break;
	}

	if(in_circle)
	{
		if(global_normal.y < 0.1)
		{
			ALBEDO = vec3(0.0, 1.0, 0.0);
		}
		else
		{
			ALBEDO = vec3(1.0, 0.0, 0.0);
		}
	}
	else
	{
		ALBEDO = vec3(0.0, 0.0, 1.0);
	}
	ALPHA = 1.0;
}
