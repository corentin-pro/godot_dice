extends Spatial


const MAX_LIGHTS = 5
const MAX_CAMERA_ANGLE = 80.0 * PI / 180.0
const DICE_ROTATIONS = [
	Quat.IDENTITY, Quat(-1, 1, 0, 0),
	Quat(-1, 1, 1, 1), Quat(1, 1, -1, 1),
	Quat(1, 1, 1, 1), Quat(1, 0, 0, 0)]

var main_dice: RigidBody = null
var additional_dice: RigidBody = null
var rng = RandomNumberGenerator.new()

var generator_mode = false
var segmentation_mode = false
var detection_mode = false
var piloted = false
var dice_count = 0
var min_camera_distance = 0.2
var max_camera_distance = 0.4
var difficulty_ratio = 1.0

var dice_size = 0.0
var face_materials = []
var dice_color = Color.white
var dice_circle_color = Color.white
var dice_top_faces: Array = [0, 0]
var corners = []

var start_time = 0
var lights = []
var included_lights = 0
var background_color: Color

onready var light_res = preload("res://light.tscn")
onready var dice_res = preload("res://dice.tscn")
onready var dice_shader = load("res://dice_face.shader")
onready var dice_shader_seg = load("res://dice_seg.shader")
onready var table_mesh = $Table/MeshInstance
onready var table_material = load("res://table.material")
onready var screen_filter = load("res://screen_filter.material")
onready var screen_shader = load("res://screen_blur.shader")

onready var env = $WorldEnvironment
onready var camera = $Camera
onready var table = $Table


func _ready() -> void:
	PhysicsServer.set_active(false)
	rng.randomize()

	for i in range(1, 7):
		face_materials.append(load('res://dice_face_%d.material' % [i]))
	main_dice = dice_res.instance()
	main_dice.gravity_scale = 0
	dice_size = main_dice.get_child(0).mesh.size.x / 2.0
	add_child(main_dice)

	additional_dice = dice_res.instance()
	additional_dice.gravity_scale = 0
	add_child(additional_dice)

	for _i in range(MAX_LIGHTS):
		lights.append(light_res.instance())

	shuffle_lights()
	shuffle_table()
	shuffle_dice(main_dice)
	shuffle_dice(additional_dice, false)

func _input(event) -> void:
	if piloted:
		return
	if event is InputEventKey:
		if event.is_pressed() and !event.is_echo():
			match event.scancode:
				KEY_E:
					camera.fov -= 1
					prints('FOV', camera.fov)
				KEY_R:
					camera.fov += 1
					prints('FOV', camera.fov)
				KEY_M:
					Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
				KEY_ENTER:
					toggle_generator()
				KEY_SPACE:
					shuffle_all()
				KEY_ESCAPE:
					if generator_mode:
						toggle_generator()
					get_tree().quit()
				KEY_V:
					toggle_segmentation()


func _process(_delta: float) -> void:
	if !piloted and generator_mode:
		shuffle_all()


func shuffle_all():
	shuffle_lights()
	shuffle_table()
	shuffle_dice(main_dice)
	shuffle_dice(additional_dice, false)
	shuffle_camera()


func get_corners():
	var additional_dice_visible = (not camera.is_position_behind(additional_dice.transform.origin)
		&& additional_dice.get_child(8).is_on_screen())
	if additional_dice_visible:
		corners = [[], []]
	else:
		corners = [[]]
	for x_offset in [-dice_size, dice_size]:
		for y_offset in [-dice_size, dice_size]:
			for z_offset in [-dice_size, dice_size]:
				corners[0].append(camera.unproject_position(
					main_dice.global_transform.origin + Vector3(
						x_offset, y_offset, z_offset).rotated(
							Vector3.UP, main_dice.rotation.y)))
				if additional_dice_visible:
					corners[1].append(camera.unproject_position(
						additional_dice.global_transform.origin + Vector3(
							x_offset, y_offset, z_offset).rotated(
								Vector3.UP, additional_dice.rotation.y)))


func toggle_segmentation() -> void:
	segmentation_mode = !segmentation_mode
	if segmentation_mode:
		env.environment.background_color = Color.black
		for i in range(6):
			face_materials[i].shader = dice_shader_seg
	else:
		env.environment.background_color = background_color
		for i in range(6):
			face_materials[i].shader = dice_shader
	table.visible = not segmentation_mode


func toggle_generator() ->void:
	if generator_mode:
		var stop_time = OS.get_ticks_msec()
		print('%d dice generated in %.02fs' % [
			dice_count, float(stop_time - start_time) / 1000])
		print('Speed : %.02f dice/s' % [
			float(dice_count * 1000) / float(stop_time - start_time)])
		if not piloted:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		OS.vsync_enabled = true
		camera.active = true
		generator_mode = false
	else:
		print("Start generating")
		if not piloted:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		OS.vsync_enabled = false
		camera.active = false
		dice_count = 0
		start_time = OS.get_ticks_msec()
		generator_mode = true


func shuffle_camera() -> void:
	var camera_distance = rng.randf_range(min_camera_distance, max_camera_distance)
	var camera_offset = Vector3(0.0, camera_distance, 0.0)
	var top_location = main_dice.transform.origin + Vector3(0.0, dice_size, 0.0)
	camera_offset = camera_offset.rotated(
		Vector3.FORWARD, rng.randf_range(0, MAX_CAMERA_ANGLE * difficulty_ratio))
	camera.transform.origin = top_location + camera_offset
	camera.fov = rng.randf_range(60.0, 85.0)
	camera.rotation = Vector3.ZERO

	var look_offset = Vector3(
		rng.randf_range(-0.3 * camera_distance, 0.3 * camera_distance),
		rng.randf_range(-0.3 * camera_distance, 0.3 * camera_distance),
		rng.randf_range(-0.3 * camera_distance, 0.3 * camera_distance))
	if (camera.transform.origin.x == main_dice.transform.origin.x) && (
			camera.transform.origin.z == main_dice.transform.origin.z):
		camera.look_at(main_dice.transform.origin + look_offset, Vector3.FORWARD)
	else:
		camera.look_at(main_dice.transform.origin + look_offset, Vector3.UP)

	var center_projection = camera.unproject_position(main_dice.transform.origin)
#	var recast = 0
	while (camera.is_position_behind(top_location)
			|| center_projection.x < get_viewport().size.x * 0.3
			|| center_projection.x > get_viewport().size.x * 0.7
			|| center_projection.y < get_viewport().size.x * 0.3
			|| center_projection.y > get_viewport().size.y * 0.7):
#		recast += 1
		look_offset = Vector3(
			rng.randf_range(-0.3 * camera_distance, 0.3 * camera_distance),
			rng.randf_range(-0.3 * camera_distance, 0.3 * camera_distance),
			rng.randf_range(-0.3 * camera_distance, 0.3 * camera_distance))
		if (camera.transform.origin.x == main_dice.transform.origin.x) && (
				camera.transform.origin.z == main_dice.transform.origin.z):
			camera.look_at(main_dice.transform.origin + look_offset, Vector3.FORWARD)
		else:
			camera.look_at(main_dice.transform.origin + look_offset, Vector3.UP)
		center_projection = camera.unproject_position(main_dice.transform.origin)
#	prints('recast', recast)
	camera.rotation.z = rng.randfn(0.0, 0.03)

	env.environment.adjustment_brightness = rng.randf_range(0.7, 1.2)
	env.environment.adjustment_contrast = rng.randf_range(0.7, 1.5)

	screen_filter.set_shader_param("blur_amount", rng.randf_range(0.0, .7))
	screen_filter.set_shader_param("noise", rng.randf_range(0.0, 0.2 * difficulty_ratio))


func shuffle_table() -> void:
	table_material.set_shader_param('period', rng.randf())
	table_material.set_shader_param('albedo', Color.from_hsv(
		rng.randf(),
		rng.randf(),
		rng.randf_range(0.1, 0.6)))


func shuffle_lights() -> void:
	background_color = Color.from_hsv(
			rng.randf(),
			rng.randf(),
			rng.randf())
	if not segmentation_mode:
		env.environment.background_color = background_color
	env.environment.ambient_light_color = Color.from_hsv(
		rng.randf(),
		rng.randf_range(0, 0.7),
		rng.randf_range(0.8, 1))
	env.environment.ambient_light_energy = rng.randf_range(0.05, 0.6)

	var light_count = rng.randi_range(1, MAX_LIGHTS)
	if light_count < included_lights:
		for i in range(light_count, included_lights):
			remove_child(lights[i])
	elif light_count > included_lights:
		for i in range(included_lights, light_count):
			add_child(lights[i])

	included_lights = light_count
	for i in range(light_count): 
		var light: OmniLight = lights[i]
		light.light_energy = rng.randf_range(0.2, 1)
		light.light_color = Color.from_hsv(
			rng.randf(),
			rng.randf(),
			rng.randf_range(0.7, 1))
		light.transform.origin = Vector3(
			rng.randf_range(-5, 5),
			rng.randf_range(0.8, 3),
			rng.randf_range(-5, 5))


func shuffle_dice(dice: RigidBody, is_main: bool = true) -> void:
	if is_main:
		dice.transform.origin = Vector3(0, 0.096, 0)
	dice.rotation = Vector3.ZERO
	var top_face = rng.randi_range(1, 6)
	dice.transform.basis = Basis(
		dice.transform.basis.get_rotation_quat() * DICE_ROTATIONS[top_face - 1])
	dice.rotate_y(rng.randf_range(0, 2 * PI))
	if not is_main:
		dice.transform.origin = Vector3(
			(1.0 if rng.randi_range(0, 1) else -1.0) * rng.randf_range(0.191, 1.2),
			0.096,
			(1.0 if rng.randi_range(0, 1) else -1.0) * rng.randf_range(0.191, 1.2))
		dice_top_faces[1] = top_face
	else:
		dice_top_faces[0] = top_face

	dice_color = Color.from_hsv(
		rng.randf(),
		rng.randf(),
		rng.randf(),
		rng.randf_range(1.0 - (0.4 * difficulty_ratio), 1))
	dice_circle_color = Color.from_hsv(
		rng.randf(),
		rng.randf(),
		rng.randf_range(0.3, 1.0),
		rng.randf_range(0.9, 1))
	while (Vector3(dice_color.r, dice_color.g, dice_color.b) - Vector3(
			dice_circle_color.r, dice_circle_color.g, dice_circle_color.b)).length() < (
				0.2 - (difficulty_ratio * 0.1)):
		dice_circle_color = Color.from_hsv(
			rng.randf(),
			rng.randf(),
			rng.randf_range(0.3, 1.0),
			rng.randf_range(0.9, 1))

	var noise_color = Color.from_hsv(
		rng.randf(),
		rng.randf(),
		rng.randf(), 1)
	var circle_radius = rng.randf_range(0.07 + (0.1 * (1.0 - difficulty_ratio)), .17)
	var circle_distance = rng.randf_range(circle_radius * 6.0, 1.0)
	for face_material in face_materials:
		face_material.set_shader_param(
			'period', rng.randf() * (1.0 + (1.0 - difficulty_ratio) * 100.0))
		face_material.set_shader_param('persistence', rng.randf())
		face_material.set_shader_param('lacunarity', rng.randf_range(0.0, 2.0))
		face_material.set_shader_param('albedo', dice_color)
		face_material.set_shader_param('noise_albedo', noise_color)
		face_material.set_shader_param('circle_radius', circle_radius)
		face_material.set_shader_param('circle_distance', circle_distance)
		face_material.set_shader_param('circle_albedo', dice_circle_color)

	dice_count += 1
